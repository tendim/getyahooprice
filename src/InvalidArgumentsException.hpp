/*
 * NoPriceOnDateException.hpp
 *
 *  Created on: May 10, 2015
 *      Author: tendim
 */

#ifndef INVALIDARGUMENTSEXCEPTION_HPP_
#define INVALIDARGUMENTSEXCEPTION_HPP_
#include <exception>

namespace part {
namespace getyahooprice {

class InvalidArgumentsException : public std::runtime_error {
public:
	InvalidArgumentsException(const std::string &r_strInvalidArgument) :
		runtime_error("invalid argument \"" + r_strInvalidArgument + "\"") {};
//	virtual ~NoPriceOnDateException() {};
};

} /* namespace getyahooprice */
} /* namespace part */

#endif /* INVALIDARGUMENTSEXCEPTION_HPP_ */
