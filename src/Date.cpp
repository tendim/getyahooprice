/*
 * Date.cpp
 *
 *  Created on: May 10, 2015
 *      Author: tendim
 */

#include "Date.hpp"
#include "InvalidArgumentsException.hpp"
#include "HelperFunctions.hpp"

namespace part {
	namespace getyahooprice {

		Date::Date()
		{
			year=0;
			month=0;
			day=0;
		}

		std::string Date::to_string() const
		{
			std::string s = pad_l(std::to_string(year), '0', 4);
			s += "-";
			s +=  pad_l(std::to_string(month), '0', 2);
			s += "-";
			s += pad_l(std::to_string(day), '0', 2);

			return s;
		}

		void Date::set(const unsigned int &r_uintYear, const unsigned int &r_uintMonth, const unsigned int &r_uintDay)
		{
			year = r_uintYear;
			month = r_uintMonth;
			day = r_uintDay;
		}

		/**
		 * Given an input string, find the positions of the two separators.
		 * \param r_strInput the input string
		 * \param r_intSep1 position of the first separator
		 * \param r_intSep2 position of the second separator
		 * \param r_charSep the separator character
		 * \return true on success, false on failure
		 *
		 * Failure will occur if: (1) both separators were not found; (2) more than two separators were found
		 */
		bool Date::getSeparators(const std::string &r_strInput, unsigned int &r_intSep1, unsigned int &r_intSep2, const char &r_charSep)
		{
			unsigned int intSep1;
			unsigned int intSep2;

			// get the first separator
			intSep1 = r_strInput.find_first_of(r_charSep);
			intSep2 = r_strInput.find_first_of(r_charSep,intSep1+1);

			// if there are more separators, error
			if (r_strInput.find_first_of(r_charSep,intSep2+1)!= std::string::npos)
			{
				throw InvalidArgumentsException(r_strInput);
			}

			if ((intSep2 > intSep1) && (intSep1 > 0))
			{
				r_intSep1 = intSep1;
				r_intSep2 = intSep2;
				return true;
			}
			else
			{
				return false;
			}
		}

		bool Date::isEmpty() const
		{
			return year==0 || 0==month || 0==day;
		}
		void Date::set(const std::string &r_strInput)
		{
			// if the passed date was the string literal, "today", then
			// use todays date
			if (string_to_upper(r_strInput) == "TODAY")
			{
				// this just calls nAV(date) with todays date..

				time_t t = time(0);
				struct tm * now = localtime(&t);
				set(now->tm_year+1900,now->tm_mon+1,now->tm_mday);
			}
			else
			{
				unsigned int intSep1=0;
				unsigned int intSep2=0;

				// try it with "/"
				if (!getSeparators(r_strInput, intSep1, intSep2, '/'))
				{
					// if we failed, try it with "-"
					if (!getSeparators(r_strInput, intSep1, intSep2, '-'))
					{
						throw InvalidArgumentsException(r_strInput);
					}
				}

//				// get the first separator
//				intSep1 = r_strInput.find_first_of("/");
//				intSep2 = r_strInput.find_first_of("/",intSep1+1);
//
//				// if there are more separators, error
//				if (r_strInput.find_first_of("/",intSep2+1)!= std::string::npos)
//				{
//					throw InvalidArgumentsException(r_strInput);
//				}

				// get the first portion
				std::string strPart1 = r_strInput.substr(0,intSep1);
				std::string strPart2 = r_strInput.substr(intSep1+1,intSep2-intSep1);
				std::string strPart3 = r_strInput.substr(intSep2+1);

				year = stringToInt(strPart1);
				month = stringToInt(strPart2);
				day = stringToInt(strPart3);

			}
		}


		bool Date::operator == (const Date &r_other) const
		{
			return year==r_other.year && month==r_other.month && day==r_other.day;
		}

		bool Date::operator != (const Date &r_other) const
		{
			return !(r_other==(*this));
		}
		bool Date::operator < (const Date &r_other) const
		{
			// if our year is before r_other year, we are definitely earlier..TRUE
			if (year < r_other.year) return true;

			// if our year is after r_other year, we are definitely not less..FALSE
			if (year > r_other.year) return false;

			// at this point, we are in the same year.. check by the months.
			// our month is before r_other month, OR months are the same, and our day is before r_other day
			return month < r_other.month ||
					((month == r_other.month) && (day < r_other.day));

		}

		bool Date::operator <= (const Date &r_other) const
		{
			return (*this)==r_other || (*this) < r_other;
		}

		bool Date::operator >= (const Date &r_other) const
		{
			return (*this)==r_other || (*this) > r_other;
		}

		bool Date::operator > (const Date &r_other) const
		{
			return !(*this < r_other) && (*this != r_other);
		}


	} /* namespace getyahooprice */
} /* namespace part */
