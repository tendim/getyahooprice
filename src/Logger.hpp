/*
 * Logger.hpp
 *
 *  Created on: May 10, 2015
 *      Author: tendim
 */

#ifndef LOGGER_HPP_
#define LOGGER_HPP_
#include <iostream>

#define LOG(s,m) part::getyahooprice::Logger::theInstance().log(s,m);
#define LOGL(l,s,m) part::getyahooprice::Logger::theInstance().log(s,m);

namespace part {
namespace getyahooprice {

class Logger {
public:
	static Logger &theInstance();
	void enable();
	void disable();
	void log(const std::string &, const std::string &);
private:
	Logger();
	virtual ~Logger();
	bool bEnabled;
};

} /* namespace getyahooprice */
} /* namespace part */

#endif /* LOGGER_HPP_ */
