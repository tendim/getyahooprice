/*
 * Tokenizer.cpp
 *
 *  Created on: Oct 23, 2014
 *      Author: tendim
 */

#include "Tokenizer.hpp"
#include "HelperFunctions.hpp"
#include <iostream>
#include "Logger.hpp"

Tokenizer::Tokenizer(
		const std::string &v_strTokens,
		const std::string &v_strQuoteCharacters)
{

	_strTokens = v_strTokens;
	_strQuoteChars = v_strQuoteCharacters;
	LOGL(1,"Tokenizer::Tokenizer()", "tokens=\"" + _strTokens + "\"");
;
}

Tokenizer::~Tokenizer()
{

}

bool Tokenizer::isQuoteCharacter(const char &r_toCheck) const
{
	return _strQuoteChars.find(r_toCheck) != std::string::npos;
}

std::string Tokenizer::returnWithoutQuoteChars(const std::string &r_toCheck)
{
	/* check if first & last characters are equal, AND they are the
	 * quote character.  if so, strip them.
	 */
	if (r_toCheck.length() < 2)
	{
		return r_toCheck;
	}
	else
	{
		if (r_toCheck[0]==r_toCheck[r_toCheck.length()-1] && isQuoteCharacter(r_toCheck[0]))
		{
			return r_toCheck.substr(1,r_toCheck.length()-2);
		}
		else
		{
			return r_toCheck;
		}
	}

}

std::vector<std::string> Tokenizer::tokenize(const std::string &v_strToTokenize)
{
	std::vector<std::string> v; // = new std::vector<std::string>;

	int i=0, j=0;
	bool bQuoteMode=false;

	LOGL(1, "Tokenizer::tokenize()", "tokenizing \"" + v_strToTokenize + "\"");

	for (j=0; j < v_strToTokenize.length(); ++j)
	{
		// check if we are in quote mode.  If so, and we have *not* found another quote
		// character, just keep going.

		if (isQuoteCharacter(v_strToTokenize.at(j)))
		{
			// flip the quote mode flag
			bQuoteMode = !bQuoteMode;
		}
		else if (bQuoteMode)
		{
			// do nothing in quote mode..
		}
		else
		{

			//check if the current character is in the list of delimiters
			if (std::string::npos != _strTokens.find(v_strToTokenize.at(j)) )
			{
				std::string toAdd = v_strToTokenize.substr(i,j-i);
				v.push_back(returnWithoutQuoteChars(toAdd));
				i=j+1;
			}
		}
	}



	// wrap up and push the last element
	if (i!=j)
	{


		v.push_back(returnWithoutQuoteChars(v_strToTokenize.substr(i)));

	}

	LOGL(1,"Tokenizer::tokenize()", "return token contents (" + std::to_string(v.size()) + " elements)");

	return v;
}
