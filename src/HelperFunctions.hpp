/*
 * HelperFunctions.hpp
 *
 *  Created on: Oct 25, 2014
 *      Author: tendim
 */

#ifndef HELPERFUNCTIONS_HPP_
#define HELPERFUNCTIONS_HPP_
#include <string>
#include <algorithm>
#include <functional>
#include <cctype>
#include <locale>
#include <fstream>
#include <vector>
#define K_ROUNDING_PRECISION 5
#define HELPER_ARG_NOT_FOUND -1

#define BOOL_TO_STRING(b) (b ? std::string("true") : std::string("false"))

unsigned int string_to_uint(const std::string &);
int stringToInt (const std::string &);
double string_to_double (const std::string &);
double stringToDouble(const std::string &);
std::string ltrim(const std::string &);
std::string rtrim(const std::string &);
std::string trim(const std::string &);
std::string double_to_percent_string(const double &r_toConvert, const unsigned int &r_intDecimalPlaces, const bool &bNegAsParenthesis);
std::string double_to_string(const double &r_toConvert, const unsigned int &r_intDecimalPlaces, const bool &bNegAsParenthesis);
std::string pad_l(const std::string &r_strToPad, const char &, const int &r_intTargetLength);
std::string pad_l(const std::string &r_strToPad, const int &r_intTargetLength);
std::string pad_r(const std::string &r_strToPad, const char &, const int &r_intTargetLength);
std::string pad_r(const std::string &r_strToPad, const int &r_intTargetLength);
double round_double(const double &);
std::string truncate_pad_r(const std::string &r_strToPad, const char &, const int &r_intTargetLength);
std::string truncate_pad_l(const std::string &r_strToPad, const char &, const int &r_intTargetLength);


/**
 * Gets the next element from a vector, based on its iterator.
 *
 * This will advance the passed iterator by one, and store the value in the next position in r_strArgument,
 * returning TRUE on success.  If advancing the iterator reaches the end of the vector, FALSE is returned
 * (i.e. there is no value to return, and r_strArgument is undefined).  If r_iterator is already out of
 * bounds, the behaviour is undefined.
 *
 * \param r_vec The vector to iterate through
 * \param r_iterator The iterator into the vector.  On exit, will point to the next position (i.e.
 * the element holding r_strArgument)
 * \param r_strArgument placeholder for the next item
 * \return TRUE on success, FALSE on failure
 */
bool getNextVectorElement(const std::vector<std::string> &r_vec, std::vector<std::string>::const_iterator &r_iterator, std::string &r_strArgument);

bool file_exists(const std::string &);
int get_arg(const std::vector<std::string> &, const std::string &);
void string_to_upper_emplace(std::string &);
std::string string_to_upper(const std::string &);

/**
 * Converts a string to a bool.  This will convert "true" or "1" to boolean TRUE and "false" or "0" to boolean FALSE; it will ignore case.
 * \param r_strToConvert the text to convert
 * \return TRUE or FALSE
 * \throw InvalidArgumentException if the string is none of 0, 1, "true", or "false"
 */
bool string_to_bool(const std::string &r_strToConvert);



#endif /* HELPERFUNCTIONS_HPP_ */
