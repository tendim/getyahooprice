/*
 * NoPriceOnDateException.hpp
 *
 *  Created on: May 10, 2015
 *      Author: tendim
 */

#ifndef NOPRICEONDATEEXCEPTION_HPP_
#define NOPRICEONDATEEXCEPTION_HPP_
#include <exception>

namespace part {
namespace getyahooprice {

class NoPriceOnDateException : public std::runtime_error {
public:
	NoPriceOnDateException(const std::string &r_strTicker, const Date &r_dtStartDate, const Date &r_dtEndDate) :
		runtime_error("no price for " + r_strTicker + " on range " + r_dtStartDate.to_string() + " to " + r_dtEndDate.to_string()) {};

	NoPriceOnDateException(const std::string &r_strTicker) :
		runtime_error("no price for " + r_strTicker) {};


	NoPriceOnDateException() :
		runtime_error("no price") {};
//	virtual ~NoPriceOnDateException() {};
};

} /* namespace getyahooprice */
} /* namespace part */

#endif /* NOPRICEONDATEEXCEPTION_HPP_ */
