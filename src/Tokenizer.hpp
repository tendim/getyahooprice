/*
 * Tokenizer.hpp
 *
 *  Created on: Oct 23, 2014
 *      Author: tendim
 */

#ifndef TOKENIZER_HPP_
#define TOKENIZER_HPP_

#include <vector>
#include <string>

#define K_TOK_DEFAULT_TOKENS " \t"

class Tokenizer {
public:
	/**
	 * Creates a Tokenizer object.
	 *
	 * \param r_strTokens characters which qualify as tokens
	 * \param r_strQuoteCharacters characters which qualify as quotes
	 */
	Tokenizer(const std::string &r_strTokens, const std::string &r_strQuoteCharacters);
	virtual ~Tokenizer();

	// TODO: fix this so that it returns std::vector<std::string> and *NOT* a pointer
	std::vector<std::string> tokenize(const std::string &);

protected:
	std::string class_name() const { return "Tokenizer"; }
	bool isQuoteCharacter(const char &) const;
	std::string returnWithoutQuoteChars(const std::string &r_toCheck);

private:
	std::string _strTokens;
	std::string _strQuoteChars;
};

#endif /* TOKENIZER_HPP_ */
