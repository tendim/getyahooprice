/*
 * GetYahooData.hpp
 *
 *  Created on: Jan 20, 2015
 *      Author: tendim
 */

#ifndef DATALAYER_GETYAHOODATA_HPP_
#define DATALAYER_GETYAHOODATA_HPP_
#include "Date.hpp"
#include <map>
#include <string>

namespace part
{
	namespace getyahooprice
	{

		class GetYahooData {
		public:
			GetYahooData();
			virtual ~GetYahooData();

			double getLatestPrice(const std::string &, Date &);
			double getPrice(const std::string &, const Date &);
			std::map<Date, double> getPrice(const std::string &, const Date &, const Date &);
		private:
			std::string buildURL(const std::string &r_strSymbol, const Date &, const Date &);
			static size_t WriteMemoryCallback(void *contents, size_t size, size_t nmemb, void *userp);
			//double getPriceByURL(const std::string &, const bool &);
			std::string getURLData(const std::string &);

		protected:
			std::string class_name() const;


		};


	}
}

#endif /* DATALAYER_GETYAHOODATA_HPP_ */
