/*
 * Logger.cpp
 *
 *  Created on: May 10, 2015
 *      Author: tendim
 */

#include "Logger.hpp"

namespace part {
namespace getyahooprice {

Logger::Logger() {
	bEnabled = false;
}

Logger::~Logger() {
}

Logger & Logger::theInstance()
{
	static Logger l;
	return l;
}


void Logger::enable()
{
	bEnabled= true;
}
void Logger::disable()
{
	bEnabled=false;
}
void Logger::log(const std::string &r_strSource, const std::string &r_strMessage)
{
	if (bEnabled)
	{
		std::clog << r_strSource << ": " << r_strMessage << std::endl;
	}
}

} /* namespace getyahooprice */
} /* namespace part */
