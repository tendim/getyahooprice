/*
 * HelperFunctions.cpp
 *
 *  Created on: Oct 25, 2014
 *      Author: tendim
 */

#include "HelperFunctions.hpp"
#include <sstream>
#include <string>
#include <iostream>
#include <math.h>
#include <algorithm>
#include <cmath>
#include <math.h>

bool file_exists(const std::string &r_strToCheck)
{
	std::ifstream ifs(r_strToCheck);
	return ifs.good();
}

int get_arg(const std::vector<std::string> &r_vecArgs, const std::string &r_strToFind)
{
	for (unsigned int i=0; i < r_vecArgs.size(); ++i)
	{
		if (r_vecArgs[i]==r_strToFind) return i;
	}
	return HELPER_ARG_NOT_FOUND;
}

double round_double(const double &r_toRound)
{
	return round(r_toRound * (pow(10,K_ROUNDING_PRECISION))) / pow (10,K_ROUNDING_PRECISION);
}

unsigned int string_to_uint (const std::string &r_strToConvert)
{
	unsigned int i;
	if (r_strToConvert.size()==0)
	{
			return 0;
	}
	else	if(!(std::istringstream(r_strToConvert)>>i))
	{
		throw std::runtime_error("std::string to uint on input \"" + r_strToConvert + "\"");
	}
	else
	{
		return i;
	}


}

int stringToInt (const std::string &r_strToConvert)
{
	int i;
	if (r_strToConvert.size()==0)
	{
			return 0;
	}
	else	if(!(std::istringstream(r_strToConvert)>>i))
	{
		throw std::runtime_error("std::string to int on input \"" + r_strToConvert + "\"");
	}
	else
	{
		return i;
	}

}



double string_to_double (const std::string &r_strToConvert)
{
	double d;
	if (r_strToConvert.size()==0)
	{
			return 0;
	}
	else	if(!(std::istringstream(r_strToConvert)>>d))
	{
		throw std::runtime_error("std::string to double on input \"" + r_strToConvert + "\"");
	}
	else
	{
		return d;
	}

}

std::string double_to_string(const double &r_toConvert, const unsigned int &r_intDecimalPlaces, const bool &r_bNegAsParenthesis)
{

	double dblScaled;
	double dblToConvert = r_toConvert;
	int intDecimalPlacement;

	// +2 because later we will *100, and +1 b/c Nth decimal place means adding .5 to the N+1th decimal place
	double dblRounder = (5.0/pow(10,1+r_intDecimalPlaces));

	dblToConvert += ((dblToConvert < 0 ? -1 : 1) * dblRounder);
	dblScaled = dblToConvert * (dblToConvert < 0 ? -1 : 1);
	std::string strConverted = std::to_string(dblScaled);

	intDecimalPlacement = strConverted.find_first_of(".",0);
	if (intDecimalPlacement != std::string::npos)
	{
		if (strConverted.length()-intDecimalPlacement > r_intDecimalPlaces)
		{
			strConverted = strConverted.substr(0, intDecimalPlacement+r_intDecimalPlaces+1);
		}
	}

	// treat negative numbers
	if (dblToConvert < 0)
	{
		if (r_bNegAsParenthesis)
		{
			strConverted = "(" + strConverted + ")";
		}
		else
		{
			strConverted = "-" + strConverted;
		}
	}
	return strConverted;

}

std::string double_to_percent_string(const double &r_toConvert, const unsigned int &r_intDecimalPlaces, const bool &r_bNegAsParenthesis)
{

	double dblScaled;
	double dblToConvert = r_toConvert;
	int intDecimalPlacement;

	// +2 because later we will *100, and +1 b/c Nth decimal place means adding .5 to the N+1th decimal place
	double dblRounder = (5.0/pow(10,2+1+r_intDecimalPlaces));

	dblToConvert += ((dblToConvert < 0 ? -1 : 1) * dblRounder);
	dblScaled = 100 * dblToConvert * (dblToConvert < 0 ? -1 : 1);
	std::string strConverted = std::to_string(dblScaled);

	intDecimalPlacement = strConverted.find_first_of(".",0);
	if (intDecimalPlacement != std::string::npos)
	{
		if (strConverted.length()-intDecimalPlacement > r_intDecimalPlaces)
		{
			strConverted = strConverted.substr(0, intDecimalPlacement+r_intDecimalPlaces+1);
		}
	}

	strConverted += "%";

	// treat negative numbers
	if (dblToConvert < 0)
	{
		if (r_bNegAsParenthesis)
		{
			strConverted = "(" + strConverted + ")";
		}
		else
		{
			strConverted = "-" + strConverted;
		}
	}
	return strConverted;

}


/**
 * Gets the next element from a vector, based on its iterator.
 *
 * This will advance the passed iterator by one, and store the value in the next position in r_strArgument,
 * returning TRUE on success.  If advancing the iterator reaches the end of the vector, FALSE is returned
 * (i.e. there is no value to return, and r_strArgument is undefined).  If r_iterator is already out of
 * bounds, the behaviour is undefined.
 *
 * \param r_vec The vector to iterate through
 * \param r_iterator The iterator into the vector.  On exit, will point to the next position (i.e.
 * the element holding r_strArgument)
 * \param r_strArgument placeholder for the next item
 * \return TRUE on success, FALSE on failure
 */
bool getNextVectorElement(const std::vector<std::string> &r_vec, std::vector<std::string>::const_iterator &r_iterator, std::string &r_strArgument)
{
	if (r_iterator==r_vec.end()) return false;

	++r_iterator;
	if (r_iterator == r_vec.end())
	{
		--r_iterator;
		return false;
	}
	else
	{
		r_strArgument= *r_iterator;
		return true;
	}
}

double stringToDouble(const std::string &r_strToConvert)
{
	double d;
	if (r_strToConvert.size()==0)
	{
		return 0;
	}
	else if (!(std::istringstream(r_strToConvert)>>d))
	{
		throw std::runtime_error("std::string to double on input \"" + r_strToConvert + "\"");
	}
	else
	{
		return d;
	}
}

// trim from start
std::string ltrim(const std::string &r_s) {
	std::string s(r_s);
	s.erase(s.begin(), std::find_if(s.begin(), s.end(), std::not1(std::ptr_fun<int, int>(std::isspace))));
	return s;
}

// trim from end
std::string rtrim(const std::string &r_s) {
	std::string s(r_s);
	s.erase(std::find_if(s.rbegin(), s.rend(), std::not1(std::ptr_fun<int, int>(std::isspace))).base(), s.end());
	return s;
}

// trim from both ends
std::string trim(const std::string &r_s) {

	std::string s(r_s);
	return ltrim(rtrim(s));
}


/**
 * Pad to the left of the input string.
 * \param r_strToPad
 * \param r_strPadCharacter
 * \param r_intTargetLength.
 * \return the padded string, up to r_intTargetLength
 */
std::string pad_l(const std::string &r_strToPad,  const int&r_intTargetLength)
{
	return pad_l(r_strToPad, ' ', r_intTargetLength);
}

/**
 * Pad to the left of the input string.
 * \param r_strToPad
 * \param r_strPadCharacter
 * \param r_intTargetLength.
 * \return the padded string, up to r_intTargetLength
 */
std::string pad_l(const std::string &r_strToPad, const char &r_chrPadWith, const int&r_intTargetLength)
{
	if (r_intTargetLength < 1)
	{
		throw std::runtime_error("pad_l(input,\""+std::to_string(r_chrPadWith)+"\","+std::to_string(r_intTargetLength)+")");
	}
	else if (r_strToPad.length() >= r_intTargetLength)
	{
		return r_strToPad;
	}
	else
	{
		std::string strToReturn = r_strToPad;
		return strToReturn.insert(0, r_intTargetLength-r_strToPad.length(),r_chrPadWith);

	}
}

/**
 * Pad to the right of the input string.
 * \param r_strToPad
 * \param r_strPadCharacter
 * \param r_intTargetLength.
 * \return the padded string, up to r_intTargetLength
 */
std::string pad_r(const std::string &r_strToPad, const int&r_intTargetLength)
{
	return pad_r(r_strToPad, ' ',r_intTargetLength);
}


/**
 * Pad to the right of the input string.
 * \param r_strToPad
 * \param r_strPadCharacter
 * \param r_intTargetLength.
 * \return the padded string, up to r_intTargetLength
 */
std::string pad_r(const std::string &r_strToPad, const char &r_chrPadWith, const int&r_intTargetLength)
{
	if (r_intTargetLength < 1)
	{
		throw std::runtime_error("pad_r(input,\""+std::to_string(r_chrPadWith)+"\","+std::to_string(r_intTargetLength)+")");
	}
	else if (r_strToPad.length() >= r_intTargetLength)
	{
		return r_strToPad;
	}
	else
	{
		std::string strToReturn = r_strToPad;
		return strToReturn.insert(r_strToPad.length(), r_intTargetLength-r_strToPad.length(),r_chrPadWith);
	}
}

/**
 * Pad to the left of the input string.
 * \param r_strToPad
 * \param r_strPadCharacter
 * \param r_intTargetLength.
 * \return the padded string, up to r_intTargetLength
 */
std::string truncate_pad_l(const std::string &r_strToPad, const char &r_chrPadWith, const int&r_intTargetLength)
{
	if (r_intTargetLength < 1)
	{
		throw std::runtime_error("pad_l(input,\""+std::to_string(r_chrPadWith)+"\","+std::to_string(r_intTargetLength)+")");
	}
	else if (r_strToPad.length() > r_intTargetLength)
	{
		return r_strToPad.substr(0,r_intTargetLength);
	}
	else
	{
		std::string strToReturn = r_strToPad;
		return strToReturn.insert(0, r_intTargetLength-r_strToPad.length(),r_chrPadWith);
	}
}


/**
 * Pad to the right of the input string.
 * \param r_strToPad
 * \param r_strPadCharacter
 * \param r_intTargetLength.
 * \return the padded string, up to r_intTargetLength
 */
std::string truncate_pad_r(const std::string &r_strToPad, const char &r_chrPadWith, const int&r_intTargetLength)
{
	if (r_intTargetLength < 1)
	{
		throw std::runtime_error("pad_r(input,\""+std::to_string(r_chrPadWith)+"\","+std::to_string(r_intTargetLength)+")");
	}
	else if (r_strToPad.length() > r_intTargetLength)
	{
		return r_strToPad.substr(0,r_intTargetLength);
	}
	else
	{
		std::string strToReturn = r_strToPad;
		return strToReturn.insert(r_strToPad.length(), r_intTargetLength-r_strToPad.length(),r_chrPadWith);
	}
}

bool string_to_bool(const std::string &r_strToConvert)
{
	bool b;
	std::string strToConvert(r_strToConvert);
	std::transform(strToConvert.begin(), strToConvert.end(), strToConvert.begin(), ::tolower);

	if (strToConvert=="0" || strToConvert=="false")
	{
		b=false;
	}
	else if (strToConvert=="1" || strToConvert=="true")
	{
		b=true;
	}
	else
	{
		throw std::runtime_error("string_to_bool(): \"" + r_strToConvert + "\" could not be converted");
	}
	return b;
}

void string_to_upper_emplace(std::string &r_toConvert)
{
	for (unsigned int i=0; i < r_toConvert.length(); ++i)
	{
		r_toConvert[i] = toupper(r_toConvert[i]);
	}
}

std::string string_to_upper(const std::string &r_toConvert)
{
	std::string s(r_toConvert);
	string_to_upper_emplace(s);
	return s;
}

