/*
 * Date.hpp
 *
 *  Created on: May 10, 2015
 *      Author: tendim
 */

#ifndef DATE_HPP_
#define DATE_HPP_
#include <string>

namespace part {
namespace getyahooprice {

struct Date {
	unsigned int year;
	unsigned int month;
	unsigned int day;

	std::string to_string() const;
	Date();
	void set(const std::string &);
	void set(const unsigned int&, const unsigned int&, const unsigned int&);
	bool isEmpty() const;
	bool operator == (const Date &r_other) const;
	bool operator != (const Date &r_other) const;
	bool operator <= (const Date &r_other) const;
	bool operator < (const Date &r_other) const;
	bool operator >= (const Date &r_other) const;
	bool operator > (const Date &r_other) const;
private:
	bool getSeparators(const std::string &r_strInput, unsigned int &r_intSep1, unsigned int &r_intSep2, const char &r_charSep);
};

} /* namespace getyahooprice */
} /* namespace part */

#endif /* DATE_HPP_ */
