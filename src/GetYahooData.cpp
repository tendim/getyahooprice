/*
 * GetYahooData.cpp
 *
 *  Created on: Jan 20, 2015
 *      Author: tendim
 */

/***************************************************************************
 *                                  _   _ ____  _
 *  Project                     ___| | | |  _ \| |
 *                             / __| | | | |_) | |
 *                            | (__| |_| |  _ <| |___
 *                             \___|\___/|_| \_\_____|
 *
 * Copyright (C) 1998 - 2013, Daniel Stenberg, <daniel@haxx.se>, et al.
 *
 * This software is licensed as described in the file COPYING, which
 * you should have received as part of this distribution. The terms
 * are also available at http://curl.haxx.se/docs/copyright.html.
 *
 * You may opt to use, copy, modify, merge, publish, distribute and/or sell
 * copies of the Software, and permit persons to whom the Software is
 * furnished to do so, under the terms of the COPYING file.
 *
 * This software is distributed on an "AS IS" basis, WITHOUT WARRANTY OF ANY
 * KIND, either express or implied.
 *
 ***************************************************************************/
/* Example source code to show how the callback function can be used to
 * download data into a chunk of memory instead of storing it in a file.
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "Logger.hpp"
#include "Tokenizer.hpp"
#include <curl/curl.h>
#include "HelperFunctions.hpp"
#include <exception>
#include "GetYahooData.hpp"
#include <memory>
#include "NoPriceOnDateException.hpp"
#include <assert.h>

#define YAHOO_GET_QUOTES_URL "http://real-chart.finance.yahoo.com/table.csv?"
#define YAHOO_STOCK_CURL_MEMCAP true
#if YAHOO_STOCK_CURL_MEMCAP
#define YAHOO_STOCK_CURL_FETCH_LIMIT 1024*1024*1024
#endif

namespace part
{
	namespace getyahooprice
	{

		struct MemoryStruct {
		  char *memory;
		  size_t size;
		};



		#include "GetYahooData.hpp"

		GetYahooData::GetYahooData() {
		}

		GetYahooData::~GetYahooData() {
		}

		size_t
		GetYahooData::WriteMemoryCallback(void *contents, size_t size, size_t nmemb, void *userp)
		{

			struct MemoryStruct *mem = (struct MemoryStruct *)userp;
			size_t stSourceLength = size * nmemb;
			size_t stToCopy = stSourceLength;
			size_t stCurrentSize = mem->size;
			size_t stNewSize = stCurrentSize + stToCopy + 1;

			// if we're overrun, do nothing
			if (stCurrentSize > YAHOO_STOCK_CURL_FETCH_LIMIT)
			{
				LOG("GetYahooData::WriteMemoryCallback()", "no space in buffer");
				return 0;
			}

			if (stNewSize > YAHOO_STOCK_CURL_FETCH_LIMIT)
			{
				stToCopy = YAHOO_STOCK_CURL_FETCH_LIMIT - stCurrentSize - 1;
				stNewSize = YAHOO_STOCK_CURL_FETCH_LIMIT;
			}

			LOG("GetYahooData::WriteMemoryCallback()", "max           =" + std::to_string(YAHOO_STOCK_CURL_FETCH_LIMIT));
			LOG("GetYahooData::WriteMemoryCallback()", "current length=" + std::to_string(stCurrentSize));
			LOG("GetYahooData::WriteMemoryCallback()", "source length =" + std::to_string(stSourceLength));
			LOG("GetYahooData::WriteMemoryCallback()", "copying over  =" + std::to_string(stToCopy));
			LOG("GetYahooData::WriteMemoryCallback()", "new size      =" + std::to_string(stNewSize));

			mem->memory = (char *)realloc(mem->memory, stNewSize);
			if(mem->memory == NULL) {
				/* out of memory! */
				LOG("GetYahooData::WriteMemoryCallback()", "not enough memory (realloc returned NULL)\n");
				return 0;
			}

			memcpy(&(mem->memory[stCurrentSize]), contents, stToCopy);
			mem->size = stNewSize-1;
			mem->memory[mem->size] = 0;
			LOGL(1,"GetYahooData::WriteMemoryCallback()", "buffer contains=" + std::string(mem->memory));

			return stNewSize-1;
		}


		std::string GetYahooData::class_name() const
		{
			return "GetYahooData";
		}

		std::string GetYahooData::buildURL(const std::string &r_strSymbol, const Date &r_dtStartDate, const Date &r_dtEndDate)
		{
			std::string strURL(YAHOO_GET_QUOTES_URL);

			//http://real-chart.finance.yahoo.com/table.csv?s=AAPL&a=00&b=16&c=2015&d=00&e=16&f=2015&g=d&ignore=.csv

			// ticker
			strURL += ("s=" + r_strSymbol);

			// start & end dates are the same
			// start date
			strURL += ("&a="+std::to_string(r_dtStartDate.month-1));
			strURL += ("&b="+std::to_string(r_dtStartDate.day));
			strURL += ("&c="+std::to_string(r_dtStartDate.year));

			// end date
			strURL += ("&d="+std::to_string(r_dtEndDate.month-1));
			strURL += ("&e="+std::to_string(r_dtEndDate.day));
			strURL += ("&f="+std::to_string(r_dtEndDate.year));
			strURL += "&g=d&ignore=.csv";

			return strURL;
		}

		double GetYahooData::getLatestPrice(const std::string &r_strSymbol, Date &r_dtPriceDate)
		{
#define CLASSANDFUNCTION "GetYahooData::getLatestPrice(const std::string &, Date &)"
#define GYD_GP_SYMBOLINDEX 0
#define GYD_GP_DATEINDEX 2
#define GYD_GP_PRICEINDEX 1
#define GYD_GP_DATE_YEARINDEX 2
#define GYD_GP_DATE_DAYINDEX 1
#define GYD_GP_DATE_MONTHINDEX 0
			// http://download.finance.yahoo.com/d/quotes.csv?s=AC.TO&f=sl1d1t1c1ohgv&e=.csv
			double dblPrice;
			// symbol, last price, last date, last time, day change, low, high, close, volume
			std::string strURL = "http://download.finance.yahoo.com/d/quotes.csv?s=";
			strURL += string_to_upper(r_strSymbol);
			strURL +="&f=sl1d1t1c1ohgv&e=.csv";

			std::string strData = getURLData(strURL);

			// tokenize the line; it is a CSV stream
			Tokenizer t(",", "\"");
			std::vector<std::string> vecData = t.tokenize(strData);
			LOGL(0, CLASSANDFUNCTION, "processing " + std::to_string(vecData.size()) + " elements");
			LOGL(0, CLASSANDFUNCTION, "pricepart=" + std::string(vecData[GYD_GP_PRICEINDEX]));
			LOGL(0, CLASSANDFUNCTION, "datepart =" + std::string(vecData[GYD_GP_DATEINDEX]));


			// try to parse the data
			try
			{
				t = Tokenizer("/","\"");
				std::vector<std::string> vecDateParts = t.tokenize(std::string(vecData[GYD_GP_DATEINDEX]));
				r_dtPriceDate.set(string_to_uint(std::string(vecDateParts[GYD_GP_DATE_YEARINDEX])),
						string_to_uint(std::string(vecDateParts[GYD_GP_DATE_MONTHINDEX])),
						string_to_uint(std::string(vecDateParts[GYD_GP_DATE_DAYINDEX])));
				dblPrice = string_to_double(std::string(vecData[GYD_GP_PRICEINDEX]));
			}
			catch (std::runtime_error &e)
			{
				throw NoPriceOnDateException();
			}
			// for the date, break it up by "/"

			LOGL(0, CLASSANDFUNCTION, "fetched price " + std::to_string(dblPrice) + " on date " + r_dtPriceDate.to_string());

			return dblPrice;
#undef CLASSANDFUNCTION
#undef GYD_GP_SYMBOLINDEX
#undef GYD_GP_DATEINDEX
#undef GYD_GP_PRICEINDEX
#undef GYD_GP_DATE_YEARINDEX
#undef GYD_GP_DATE_DAYINDEX
#undef GYD_GP_DATE_MONTHINDEX

		}

		double GetYahooData::getPrice(const std::string &r_strSymbol, const Date &r_dtDate)
		{
#define CLASSANDFUNCTION "GetYahooData::getPrice(const std::string &, const Date &)"
			std::map<Date, double> mapResults = getPrice(r_strSymbol, r_dtDate, r_dtDate);

			LOGL(0, CLASSANDFUNCTION, "results map has " + std::to_string(mapResults.size()) + " elements");

			for (std::map<Date, double>::const_iterator it = mapResults.begin(); it != mapResults.end(); ++it)
			{
				LOGL(1, CLASSANDFUNCTION, it->first.to_string() + ": " + std::to_string(it->second));
			}

			if (mapResults.find(r_dtDate)==mapResults.end())
			{
				LOGL(0, CLASSANDFUNCTION, "price on date " + r_dtDate.to_string() + " not found");
				throw NoPriceOnDateException();
			}
			else
			{
				double dblPrice = mapResults[r_dtDate];
				LOGL(0, CLASSANDFUNCTION, "price on date " + r_dtDate.to_string() +
						"=" + std::to_string(dblPrice));

				return dblPrice;
			}
#undef CLASSANDFUNCTION
		}

		/**
		 * \param r_strSymbol Ticker of price to fetch
		 * \param r_dtStartDate start date of range of prices
		 * \param r_dtEndDate end date of range of prices
		 *
		 */
		std::map<Date, double> GetYahooData::getPrice(const std::string &r_strSymbol, const Date &r_dtStartDate, const Date &r_dtEndDate)
		{
#define CLASSANDFUNCTION "GetYahooData::getPrice(const std::string &, const Date &, const Date &)"
#define GYD_GP_DATEINDEX 0
#define GYD_GP_PRICEINDEX 4
#define GYD_GP_LINESIZE 7
#define GYD_GP_DATE_YEARINDEX 0
#define GYD_GP_DATE_DAYINDEX 2
#define GYD_GP_DATE_MONTHINDEX 1

			assert(r_dtStartDate <= r_dtEndDate);


			std::map<Date, double> mapResults;
			std::string strData = getURLData(buildURL(r_strSymbol, r_dtStartDate, r_dtEndDate));

			// copy the results into something more manageable
			Tokenizer t("\n", "\"");
			std::vector<std::string> vecLines = t.tokenize(strData);
			LOGL(0, CLASSANDFUNCTION, "yahoo returned " + std::to_string(vecLines.size()) + " lines");


			//preset the tokenizer
			t = Tokenizer(",", "\"");
			// iterate through each line (skipping line 0 b/c it is the header)
			for (int i=1; i < vecLines.size(); ++i)
			{
				std::string strDataLine = vecLines[i];
				LOGL(0, CLASSANDFUNCTION, "processing line #" + std::to_string(i) + ": \"" + strDataLine + "\"");

				std::vector<std::string> vecLineElements = t.tokenize(strDataLine);
				LOGL(0, CLASSANDFUNCTION, "line #" + std::to_string(i) + " had " + std::to_string(vecLineElements.size()) + " elements");

				// returned line must exactly 7 elements, or there is an error
				if (vecLineElements.size() != GYD_GP_LINESIZE)
				{
					throw NoPriceOnDateException();
				}
				else
				{
					Date d;

					// parse out the date part
					Tokenizer tokDate("-", "\"");
					std::vector<std::string> vecDateParts = tokDate.tokenize(vecLineElements.at(GYD_GP_DATEINDEX));
					LOGL(0, CLASSANDFUNCTION, "vecDateParts count = " + std::to_string(vecDateParts.size()));
					LOGL(0, CLASSANDFUNCTION, "vecDateParts[GYD_GP_DATE_YEARINDEX]=\"" + vecDateParts.at(GYD_GP_DATE_YEARINDEX)+"\"");
					LOGL(0, CLASSANDFUNCTION, "vecDateParts[GYD_GP_DATE_MONTHINDEX]=\"" + vecDateParts.at(GYD_GP_DATE_MONTHINDEX)+"\"");
					LOGL(0, CLASSANDFUNCTION, "vecDateParts[GYD_GP_DATE_DAYINDEX]=\"" + vecDateParts.at(GYD_GP_DATE_DAYINDEX)+"\"");
					d.set(string_to_uint(vecDateParts.at(GYD_GP_DATE_YEARINDEX)),
							string_to_uint(vecDateParts.at(GYD_GP_DATE_MONTHINDEX)),
							string_to_uint(vecDateParts.at(GYD_GP_DATE_DAYINDEX)));

					// get the price
					double dblPrice = string_to_double(vecLineElements.at(GYD_GP_PRICEINDEX));

					// add to the map
					mapResults.insert(std::make_pair(d, dblPrice));
				}
			}


			return mapResults;
#undef CLASSANDFUNCTION
#undef GYD_GP_DATEINDEX
#undef GYD_GP_PRICEINDEX
#undef GYD_GP_LINESIZE
#undef GYD_GP_DATE_YEARINDEX
#undef GYD_GP_DATE_DAYINDEX
#undef GYD_GP_DATE_MONTHINDEX
		}

		std::string GetYahooData::getURLData(const std::string &r_strURL)
		{
#define CLASSANDFUNCTION "GetYahooData::getURLData(const std::string &)"

			LOG(CLASSANDFUNCTION, "url=\"" + r_strURL + "\"");
			CURL *curl_handle;
			CURLcode res;

			struct MemoryStruct chunk;

			chunk.memory = (char *)malloc(1);  /* will be grown as needed by the realloc above */
			chunk.size = 0;    /* no data at this point */

			curl_global_init(CURL_GLOBAL_ALL);

			/* init the curl session */
			curl_handle = curl_easy_init();

			/* specify URL to get */
			LOG(CLASSANDFUNCTION, "opening URL");
			curl_easy_setopt(curl_handle, CURLOPT_URL, r_strURL.c_str());//"http://www.example.com/");

			/* send all data to this function  */
			curl_easy_setopt(curl_handle, CURLOPT_WRITEFUNCTION, this->WriteMemoryCallback);

			/* we pass our 'chunk' struct to the callback function */
			curl_easy_setopt(curl_handle, CURLOPT_WRITEDATA, (void *)&chunk);

			/* some servers don't like requests that are made without a user-agent
			field, so we provide one */
			curl_easy_setopt(curl_handle, CURLOPT_USERAGENT, "libcurl-agent/1.0");

			/* get it! */
			res = curl_easy_perform(curl_handle);
			std::string strCSVLine(chunk.memory);
			LOGL(1,CLASSANDFUNCTION, "received the following: " + strCSVLine);
			if(chunk.memory) free(chunk.memory);

			/* cleanup curl stuff */
			curl_easy_cleanup(curl_handle);


			/* we're done with libcurl, so clean it up */
			curl_global_cleanup();

			/* check for errors */
			if(res != CURLE_OK) {
				LOG(CLASSANDFUNCTION, "failed to get data from yahoo, error code #" + std::to_string(res));
				throw NoPriceOnDateException();
			}
			else
			{
				return strCSVLine;
			}
#undef CLASSANDFUNCTION
		}

//		double GetYahooData::getPriceByURL(const std::string &r_strURL, const bool &r_bHasHeader)
//		{
//			LOG("GetYahooData::getPriceByURL()", "url=\"" + r_strURL + "\"");
//			double dblPrice=0;
//			CURL *curl_handle;
//			CURLcode res;
//
//			struct MemoryStruct chunk;
//
//			chunk.memory = (char *)malloc(1);  /* will be grown as needed by the realloc above */
//			chunk.size = 0;    /* no data at this point */
//
//			curl_global_init(CURL_GLOBAL_ALL);
//
//			/* init the curl session */
//			curl_handle = curl_easy_init();
//
//			/* specify URL to get */
//			LOG("GetYahooData::getPriceByURL()", "opening URL");
//			curl_easy_setopt(curl_handle, CURLOPT_URL, r_strURL.c_str());//"http://www.example.com/");
//
//			/* send all data to this function  */
//			curl_easy_setopt(curl_handle, CURLOPT_WRITEFUNCTION, this->WriteMemoryCallback);
//
//			/* we pass our 'chunk' struct to the callback function */
//			curl_easy_setopt(curl_handle, CURLOPT_WRITEDATA, (void *)&chunk);
//
//			/* some servers don't like requests that are made without a user-agent
//			field, so we provide one */
//			curl_easy_setopt(curl_handle, CURLOPT_USERAGENT, "libcurl-agent/1.0");
//
//			/* get it! */
//			res = curl_easy_perform(curl_handle);
//			std::string strCSVLine(chunk.memory);
//			LOGL(1,"GetYahooData::getPriceByURL()", "processing yahoo data " + strCSVLine);
//			if(chunk.memory) free(chunk.memory);
//
//			/* cleanup curl stuff */
//			curl_easy_cleanup(curl_handle);
//
//
//			/* we're done with libcurl, so clean it up */
//			curl_global_cleanup();
//
//			/* check for errors */
//			if(res != CURLE_OK) {
//				LOG("GetYahooData::getPriceByURL()", "failed to get data from yahoo");
//				throw NoPriceOnDateException();
//			}
//			else
//			{
//				/*
//				* Now, our chunk.memory points to a memory block that is chunk.size
//				* bytes big and contains the remote file.
//				*
//				* Do something nice with it!
//				*/
//
//				// copy the results into something more manageable
//				Tokenizer t("\n", "\"");
//				std::vector<std::string> vecLines = t.tokenize(strCSVLine);
//				LOGL(1, "GetYahooData::getPriceByURL()", "yahoo returned " + std::to_string(vecLines.size()) + " lines");
//
//				t=Tokenizer(",","\"");
//				unsigned int uintDataLine = r_bHasHeader ? 1 : 0;
//				if (uintDataLine >= vecLines.size())
//				{
//					throw NoPriceOnDateException();
//				}
//				std::string strDataLine = vecLines[uintDataLine];
//				LOGL(1, "GetYahooData::getPriceByURL", "dataline=\"" + strDataLine + "\"");
//				std::vector<std::string> vecElements = t.tokenize(strDataLine);
//				LOGL(1, "GetYahooData::getPriceByURL()", "line 1 had " + std::to_string(vecElements.size()) + " elements");
//
//				// returned line must exactly 7 elements, or there is an error
//				if (vecElements.size() != 7)
//				{
//					throw NoPriceOnDateException();
//				}
//
//				dblPrice = string_to_double(vecElements.at(4));
//			}
//
//
//			return dblPrice;
//
//		}

	}
}
