//============================================================================
// Name        : getyahooprice.cpp
// Author      : Patrick M. Pritchard
// Version     :
// Copyright   : (c) Patrick M. Pritchard
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>
#include "Date.hpp"
#include "HelperFunctions.hpp"
#include <ctype.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include "GetYahooData.hpp"
#include "Logger.hpp"
#include <exception>
#include "NoPriceOnDateException.hpp"

/*
int
main (int argc, char **argv)
{
  int aflag = 0;
  int bflag = 0;
  char *cvalue = NULL;
  int index;
  int c;

  opterr = 0;

  while ((c = getopt (argc, argv, "abc:")) != -1)
    switch (c)
      {
      case 'a':
        aflag = 1;
        break;
      case 'b':
        bflag = 1;
        break;
      case 'c':
        cvalue = optarg;
        break;
      case '?':
        if (optopt == 'c')
          fprintf (stderr, "Option -%c requires an argument.\n", optopt);
        else if (isprint (optopt))
          fprintf (stderr, "Unknown option `-%c'.\n", optopt);
        else
          fprintf (stderr,
                   "Unknown option character `\\x%x'.\n",
                   optopt);
        return 1;
      default:
        abort ();
      }

*/

std::string longModeOutput(const std::string &,
	const part::getyahooprice::Date &,
	const double &,
	const std::string &);

void outputPrice(
	const bool &r_bLongMode,
	const std::string &r_strTicker,
	const part::getyahooprice::Date &r_dtDate,
	const double &r_dblPrice,
	const std::string &r_strMessage);

void showhelp();

int main(int argc, char **argv) {
	std::string strTicker;
	std::string strMessage;
	std::string strYahooSymbol;
	bool bLongMode = false;
	part::getyahooprice::Date dtDate, dtStartDate, dtEndDate;
	part::getyahooprice::GetYahooData gyd;
	int c;
	bool bRange = false;
	// bool bReadFromSTDIN = false;				// flag: read ticker(s) from standard input

//	part::getyahooprice::Logger::theInstance().enable();

	/*
	 * t - ticker (accepts input)
	 * m - message (accepts input)
	 * y - yahoo symbol (accepts input)
	 * d - date or "today" (accepts input)
	 * l - long mode, otherwise only displays price
	 * v - verbose debugging output
	 * s - start date (accepts input)
	 * e - end date (accepts input)
	 */
	while ((c = getopt(argc, argv, "t:y:m:d:lv?hs:e:")) != -1)
	{
//		LOG("main()", "processing " + std::to_string(c));
		switch (c)
		{
		// get ticker
		//case 't':
		//	strTicker = string_to_upper(std::string(optarg));
		//	break;
			
		// specify yahoo ticker
		case 'y':
			strYahooSymbol = string_to_upper(std::string(optarg));
			break;
		
		// optional message to append to long output
		case 'm':
			strMessage = std::string(optarg);
			break;
		
		// single date in ISO format (yyyy-mm-dd)
		case 'd':
			dtDate.set(std::string(optarg));
			break;
			
		// date range: end
		case 'e':
			dtEndDate.set(std::string(optarg));
			break;
		
		// date range: start
		case 's':
			dtStartDate.set(std::string(optarg));
			break;
		
		// use long mode
		case 'l':
			bLongMode=true;
			break;
			
		// enable verbose output
		case 'v':
			part::getyahooprice::Logger::theInstance().enable();
			break;
			
		// show help (and quit after)
		case '?':
			showhelp();
			return 1;
		case 'h':
			showhelp();
			return 1;
			
		// unknown command ; quit
		default:
			std::cout << "unknown option, try \"-h\" for help.\n";
			return 1;
		}
	}

	// fetch the ticker from the end, but only if a ticker hasn't been
	// defined.  if one has, fail.
	if (optind < argc && ""==strTicker)
	{
		strTicker = string_to_upper(argv[optind]);
	}
	else
	{
		std::cout << "no ticker defined\n";
		return 1;
	}
	
	assert("" != strTicker);
	
	// gobs of debugging output
	LOG("main()", "ticker      =\"" + strTicker + "\"");
	LOG("main()", "message     =\"" + strMessage + "\"");
	LOG("main()", "yahoo symbol=\"" + strYahooSymbol + "\"");
	LOG("main()", "date        =" + dtDate.to_string());
	LOG("main()", "start date  =" + dtStartDate.to_string());
	LOG("main()", "end date    =" + dtEndDate.to_string());


	// if yahoo specified, but ticker not, set ticker to yahoo
//	if (""==strTicker && "" != strYahooSymbol)
//	{
//		strTicker = strYahooSymbol;
//	}

	// if no ticker was specified, and we are not reading from stdin, fail
//	if (""==strTicker)
//	{
		std::cout << "no ticker specified" << std::endl;
//		LOG("main()", "no ticker specified");
//		return 1;
//	}
	
	/*
	 * date conditions:
	 *
	 *      DATE   START   END     CONDITOIN
	 *      !empty empty   empty   OKAY
	 *      empty  empty   !empty  FAIL
	 *      empty  !empty  empty   FAIL
	 *      empty  !empty  !empty  OKAY
	 *      !empty empty   !empty  FAIL
	 *      !empty !empty  empty   FAIL
	 *      empty  empty   empty   OKAY
	 *      !empty !empty  !empty  FAIL
	 */

	double dblPrice;
	std::map<part::getyahooprice::Date, double> mapResults;
	std::string strTickerToUse = strYahooSymbol=="" ? strTicker : strYahooSymbol;

	try
	{

		// date specified
		if (!dtDate.isEmpty() && dtStartDate.isEmpty() && dtEndDate.isEmpty())
		{
			dblPrice = gyd.getPrice(strTickerToUse, dtDate);
			bRange=false;
		}

		// range specified
		else if (dtDate.isEmpty() && !dtStartDate.isEmpty() && !dtEndDate.isEmpty())
		{
			if (dtEndDate < dtStartDate)
			{
				std::cout << "end date cannot be before start date" << std::endl;
				return 1;
			}
			bRange=true;
			mapResults = gyd.getPrice(strTickerToUse, dtStartDate, dtEndDate);
		}

		// no dates specified
		// get the latest price, and save the date of that price in dtDate
		else if (dtDate.isEmpty() && dtStartDate.isEmpty() && dtEndDate.isEmpty())
		{
			dblPrice = gyd.getLatestPrice(strTickerToUse, dtDate);
			bRange=false;
		}
		else
		{
			std::cout << "ambiguous date(s) specified" << std::endl;
			return 1;
		}

	}
	
	catch (part::getyahooprice::NoPriceOnDateException &e)
	{
		dblPrice=-1;
		strMessage = "no price. " + strMessage;
	}

	if (bRange)
	{
		for (std::map<part::getyahooprice::Date, double>::const_iterator it = mapResults.begin(); it != mapResults.end(); ++it)
		{
			outputPrice(bLongMode, strTicker, it->first, it->second, strMessage);
		}
	}
	else
	{
		outputPrice(bLongMode, strTicker, dtDate, dblPrice, strMessage);
	}

	return 0;
}


void outputPrice(
	const bool &r_bLongMode,
	const std::string &r_strTicker,
	const part::getyahooprice::Date &r_dtDate,
	const double &r_dblPrice,
	const std::string &r_strMessage)
{

	if (r_bLongMode)
	{
		std::cout << longModeOutput(r_strTicker, r_dtDate, r_dblPrice, r_strMessage)
		  << std::endl;
	}
	else
	{
		std::cout << std::to_string(r_dblPrice) << std::endl;

	}
}
std::string longModeOutput(
	const std::string &r_strTicker,
	const part::getyahooprice::Date &r_dtDate,
	const double &r_dblPrice,
	const std::string &r_strMessage)
{
	std::string s;

	s = r_strTicker;
	s += ",";
	s += r_dtDate.to_string();
	s += ",";
	s += double_to_string(r_dblPrice,5,false);
	s += ",";
	s += "\"";
	s += r_strMessage;
	s += "\"";

	return s;
}

void showhelp()
{
	/*
	 * t - ticker
	 * m - message
	 * y - yahoo symbol
	 * d - date or "today"
	 * l - long mode, otherwise only displays price
	 * v - verbose debugging output
	 */
//                          12345678901234567890123456789012345678901234567890123456789012345678901234567890
	std::cout << "getyahooprice\n"
			<< "(c) patrick m pritchard <patrick@d26.net>\n"
			<< "some portions from the CURL project (c) 1998 - 2013,\n"
			<< "Daniel Stenberg, <daniel@haxx.se>, et al.\n"
			<< "\n"
			<< "will fetch prices from yahoo\n\n"
			<< "usage: getyahooprice [ -l ] [ -v ] [ -y YAHOOTICKER ] [ -m MESSAGE ] [ -d DATE | -s DATE -e DATE ] ticker | -\n"
			<< "where:\n"
//			<< "-t TICKER         the ticker to look up, in yahoo format.\n"
//			<< "                  e.g. HLF.TO for High Liner Foods\n"
			<< "-y YAHOOTICKER    if Yahoo uses a different symbol than the actual (e.g. for\n"
			<< "                  mutual funds), this symbol will be used for the actual lookup\n"
			<< "-m MESSAGE        an optional message if long output is used\n"
			<< "-d DATE           the date in ISO 8601 format, or the word \"today\"\n"
			<< "-s DATE1 -e DATE2 a date range between DATE1 and DATE2
			<< "-v                verbose mode (i.e. lots of debugging info)\n"
			<< "-l                long format\n"
			<< "\n"
			<< "if long format is specified, the output string is in the form of\n"
			<< "\n"
			<< "TICKER,DATE,PRICE,MESSAGE\n"
			<< "\n"
			<< "TICKER will match what is passed with -t.  Note that for prices where -y is\n"
			<< "passed, the -y value is used for lookup, but -t value is used for display.\n"
			<< "\n"
			<< "if long format is not used, the price alone is output.\n\n"
			<< "if no price is found, -1 is listed.  in long format, an error message will be\n"
			<< "prepended to the message\n"
			<< "\n"
			<< "if a ticker is not specified, then nothing happens\n"
			<< "\n"
			<< "if a date is not specified, the latest available price is taken from Yahoo\n";
}
